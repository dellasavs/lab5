// server.js

// BASE SETUP
// =============================================================================

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/lab5');

// app/models/Tweet.js

var Account     = require('./models/User');


// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var path       = require('path');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 3000;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    
    console.log('Middleman');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, './src', 'index.html')); 
});


// on routes that end in /bears
// ----------------------------------------------------
router.route('/api/login')

    // create an account (accessed at POST http://localhost:8080/api/bears)
    .post(function(req, res) {
    
        var myUser = new Account();      //
        myUser.username = req.body.username;  // set the User owner' name (comes from the request)
        myUser.password = req.body.password;   //set the password
        // save the bear and check for errors
        console.log('Registered');
        myUser.save(function(err) { //save the User in the database
            if(err)
                res.send(err);

            res.json({ message: 'User created' });
        });
    })
        
            // get all the users (accessed at GET http://localhost:8080/api/bears)
    .get(function(req, res) {
        Account.find(function(err, accounts) {
            if (err)
                res.send(err);

            res.json(accounts);
        });
    });
    
// on routes that end in /bears/:bear_id
// ----------------------------------------------------
router.route('/api/login/:account_username')

    // get the bear with that id (accessed at GET http://localhost:8080/api/bears/:bear_id)
    .get(function(req, res) {
        Account.findById(Account.username, function(err, account) {
            console.log("Found user");
            if (err)
                res.send(err);
            res.json({Message: 'hello world'});
        });
    })
        
    .put(function(req, res) {

        // use our bear model to find the bear we want
        User.findById(req.params.user_id, function(err, user) {

            if (err)
                res.send(err);

            Account.password = req.body.password;  // update the bears info

            // save the bear
            User.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User updated!' });
            });
        });
    })
        
    .delete(function(req, res) {
        Account.remove({
            _id: req.params.user_id
        }, function(err, user) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });
    

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api

app.use('/', router);
app.use('/public', express.static('public'));
// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);