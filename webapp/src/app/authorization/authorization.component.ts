import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from './authorization.service';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css'],
    providers: [AuthorizationService],
})
export class AuthorizationComponent implements OnInit {
  private user;
  private pass;

  constructor(private userService: AuthorizationService, private router: Router) { }

  ngOnInit() {
  }



    onSubmit() {
    this.userService.login(this.user, this.pass).subscribe((result) => {
      if (result) {
        this.router.navigate(['']);
      }
    });

}
}
